﻿namespace KinectGestures
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Diagnostics;
    using System.IO;
    using Microsoft.Kinect;

    public interface EventListener
    {
        void onGestureRegistered(Gestures a, CameraSpacePoint c);
        void onPositionChanged(Positions g);
        void onAttentionChanged(Attention a);
        void onHandPositionChanged(Hands h);
    }

    public enum Attention { LookingUp, LookingDown, LookingAway };
    public enum Gestures { GrabAndHold, RightHandSwipe, LeftHandSwipe, RightFling, LeftFling, Pinch};
    public enum Positions { BehindTable, NotBehindTable }
    public enum Hands { RightOverTable, LeftOverTable, BothOverTable , NotOverTable}

    class GestureLibrary
    {
        private class ListOfGestures
        {
            public enum Status { Tracked, NotTracked, InProgress };
            public RightHandSwipeGesture RightHandSwipe;
            public LeftHandSwipeGesture LeftHandSwipe;
            public GrabAndHoldGesture GrabAndHold;
            public FlingGesture RightFling;
            public FlingGesture LeftFling;

            public ListOfGestures()
            {
                RightHandSwipe = new RightHandSwipeGesture();
                LeftHandSwipe = new LeftHandSwipeGesture();
                GrabAndHold = new GrabAndHoldGesture();
                RightFling = new FlingGesture();
                LeftFling = new FlingGesture();
            }

            public void reset()
            {
                RightHandSwipe.reset();
                LeftHandSwipe.reset();
                GrabAndHold.Rreset();
                GrabAndHold.Lreset();
                RightFling.reset();

            }

            public class FlingGesture
            {
                public Status status;
                public CameraSpacePoint firstPoint;
                public CameraSpacePoint lastPoint;
                public float totalFrames;
                public float goodFrames;


                public FlingGesture()
                {
                    status = Status.NotTracked;
                    totalFrames = 0;
                    goodFrames = 0;
                }

                public void reset()
                {
                    status = Status.NotTracked;
                    totalFrames = 0;
                    goodFrames = 0;
                }
            }
            public class RightHandSwipeGesture
            {
                public Status status;
                public CameraSpacePoint firstPoint;
                public CameraSpacePoint lastPoint;
                public float totalFrames;
                public float goodFrames;

                public RightHandSwipeGesture()
                {
                    status = Status.NotTracked;
                }

                public void reset()
                {
                    status = Status.NotTracked;
                }
            }

            public class LeftHandSwipeGesture
            {
                public Status status;
                public CameraSpacePoint firstPoint;
                public CameraSpacePoint lastPoint;
                public float totalFrames;
                public float goodFrames;

                public LeftHandSwipeGesture()
                {
                    status = Status.NotTracked;
                }

                public void reset()
                {
                    status = Status.NotTracked;
                }
            }

            public class GrabAndHoldGesture
            {
                public Status Rstatus;
                public Status Lstatus;
                public CameraSpacePoint currentPoint;
                public int RgrabFrameCount;
                public int RopenFrameCount;
                public int LgrabFrameCount;
                public int LopenFrameCount;

                public GrabAndHoldGesture()
                {
                    Rstatus = Status.NotTracked;
                    Lstatus = Status.NotTracked;
                    RgrabFrameCount = 0;
                    RopenFrameCount = 0;
                    LgrabFrameCount = 0;
                    LopenFrameCount = 0;
                }

                public void Rreset()
                {
                    Rstatus = Status.NotTracked;
                    RgrabFrameCount = 0;
                    RopenFrameCount = 0;

                }

                public void Lreset()
                {
                    Lstatus = Status.NotTracked;
                    LgrabFrameCount = 0;
                    LopenFrameCount = 0;
                }
            }
        }

        /// <summary>
        /// Contains the status data of all supported gestures
        /// </summary>
        private static ListOfGestures gestureList;

        /// <summary>
        /// Active Kinect sensor
        /// </summary>
        private static KinectSensor kinectSensor = null;

        /// <summary>
        /// Coordinate mapper to map one type of point to another
        /// </summary>
        private static CoordinateMapper coordinateMapper = null;

        /// <summary>
        /// Reader for body frames
        /// </summary>
        private static BodyFrameReader reader = null;

        /// <summary>
        /// Array for the bodies
        /// </summary>
        private static Body[] bodies = null;

        /// <summary>
        /// Length of the table
        /// </summary>
        private static int tableLength;

        /// <summary>
        /// Depth of the table
        /// </summary>
        private static int tableDepth;

        /// <summary>
        /// Distance from Kinect to first edge of table
        /// </summary>
        private static int tableDistance;

        /// <summary>
        /// Height of the table
        /// </summary>
        private static int tableHeight;

        /// <summary>
        /// Distance the Kinect is off of the ground
        /// </summary>
        private static int kinectHeight;

        /// <summary>
        /// A stopwatch for left swipe right handgesture changes
        /// </summary>
        private static Stopwatch rightHandSwipeLeftWatch = new Stopwatch();

        /// <summary>
        /// A stopwatch for right swipe right hand gesture changes
        /// </summary>
        private static Stopwatch rightHandSwipeRightWatch = new Stopwatch();

        /// <summary>
        /// A stopwatch for left swipe gesture changes
        /// </summary>
        private static Stopwatch leftHandSwipeRightWatch = new Stopwatch();

        /// <summary>
        /// The time of the first frame received
        /// </summary>
        // private long startTime = 0;

        /// <summary>
        /// Current attention of the user
        /// </summary>
        private static Attention currentAttention;

        private static Attention lastStableAttention;

        /// <summary>
        /// A stopwatch for attention changes
        /// </summary>
        private static Stopwatch attentionWatch = new Stopwatch();

        /// <summary>
        /// True when attention variable was recently changed
        /// </summary>
        private static bool recentAttentionSwitch = false;

        /// <summary>
        /// Current state of the hands; over the table or not over the table
        /// </summary>
        private static Hands currentHandPositions = Hands.NotOverTable;

        private static Hands lastStableHandPosition = Hands.NotOverTable;

        /// <summary>
        /// A stopwatch for hand position changes
        /// </summary>
        private static Stopwatch handWatch = new Stopwatch();

        /// <summary>
        /// True when hand location variable was recently changed
        /// </summary>
        private static bool recentHandSwitch = false;

        /// <summary>
        /// A stopwatch for fling gesture changes
        /// </summary>
        private static Stopwatch rightFlingWatch = new Stopwatch();

        /// <summary>
        /// A stopwatch for pull gesture changes
        /// </summary>
        private static Stopwatch leftFlingWatch = new Stopwatch();

        /// <summary>
        /// Tracks how many frames a pinch has been held
        /// </summary>
        private static int pinchOpenRightCount = 0;

        private static int pinchClosedRightCount = 0;

        /// <summary>
        /// Tracks how many frames a pinch has been held
        /// </summary>
        private static int pinchOpenLeftCount = 0;

        private static int pinchClosedLeftCount = 0;

        private static int framesSinceGesture = 0;

        private static bool readyForGesture = true;

        private static EventListener sListener;

        public static void setListener(EventListener e)
        {
            sListener = e;
        }

        private static void setTableDimensions()
        {
            try
            {
                using (StreamReader sr = new StreamReader("../../TableDimensions.txt"))
                {
                    String line;
                    while((line = sr.ReadLine()) != null)
                    {
                        if (line.Substring(0, 6).Equals("LENGTH"))
                        {
                            tableLength = Convert.ToInt32(line.Substring(7));
                            Console.WriteLine("tableLength: {0}\n", tableLength);
                        }
                        else if (line.Substring(0, 5).Equals("DEPTH"))
                        {
                            tableDepth = Convert.ToInt32(line.Substring(6));
                            Console.WriteLine("tableDepth: {0}\n", tableDepth);
                        }
                        else if (line.Substring(0, 8).Equals("DISTANCE"))
                        {
                            tableDistance = Convert.ToInt32(line.Substring(9));
                            Console.WriteLine("tableDistance: {0}\n", tableDistance);
                        }
                        else if (line.Substring(0, 6).Equals("HEIGHT"))
                        {
                            tableHeight = Convert.ToInt32(line.Substring(7));
                            Console.WriteLine("tableHeight: {0}\n", tableHeight);
                        }
                        else if (line.Substring(0, 6).Equals("KINECT"))
                        {
                            tableHeight = Convert.ToInt32(line.Substring(7));
                            Console.WriteLine("tableHeight: {0}\n", kinectHeight);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("No table config file found:");
                Console.WriteLine(e.Message);
            }
        }

        public static bool start()
        {
            // One sensor is supported
            kinectSensor = KinectSensor.Default;

            if (kinectSensor != null)
            {
                // get the coordinate mapper
                coordinateMapper = kinectSensor.CoordinateMapper;

                // open the sensor
                kinectSensor.Open();

                // get the depth (display) extents
                FrameDescription frameDescription = kinectSensor.DepthFrameSource.FrameDescription;

                bodies = new Body[kinectSensor.BodyFrameSource.BodyCount];

                // open the reader for the body frames
                reader = kinectSensor.BodyFrameSource.OpenReader();

                gestureList = new ListOfGestures();

                setTableDimensions();

                return true;
            }
            else
            {
                return false;
            }
        }

        public static void getEvents()
        {
            if (reader != null)
            {
                reader.FrameArrived += Reader_FrameArrived;
            }
        }

        public static void close()
        {
            if (reader != null)
            {
                // BodyFrameReder is IDisposable
                reader.Dispose();
                reader = null;
            }

            // Body is IDisposable
            if (bodies != null)
            {
                foreach (Body body in bodies)
                {
                    if (body != null)
                    {
                        body.Dispose();
                    }
                }
            }

            if (kinectSensor != null)
            {
                kinectSensor.Close();
                kinectSensor = null;
            }
        }

        /// <summary>
        /// Handles the body frame data arriving from the sensor
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private static void Reader_FrameArrived(object sender, BodyFrameArrivedEventArgs e)
        {
            BodyFrameReference frameReference = e.FrameReference;

            if (readyForGesture)
            {
                try
                {
                    BodyFrame frame = frameReference.AcquireFrame();

                    if (frame != null)
                    {
                        // BodyFrame is IDisposable
                        using (frame)
                        {
                            // The first time GetAndRefreshBodyData is called, Kinect will allocate each Body in the array.
                            // As long as those body objects are not disposed and not set to null in the array,
                            // those body objects will be re-used.
                            frame.GetAndRefreshBodyData(bodies);

                            foreach (Body body in bodies)
                            {
                                if (body.IsTracked)
                                {
                                    IReadOnlyDictionary<JointType, Joint> joints = body.Joints;
                                    IReadOnlyDictionary<JointType, JointOrientation> orientation = body.JointOrientations;
                                    CameraSpacePoint headVec = joints[JointType.Head].Position;
                                    CameraSpacePoint neckVec = joints[JointType.Neck].Position;
                                    CameraSpacePoint spine_shoulderVec = joints[JointType.SpineShoulder].Position;
                                    CameraSpacePoint spine_midVec = joints[JointType.SpineMid].Position;
                                    Vector4 vec = orientation[JointType.Neck].Orientation;

                                    //Console.WriteLine("{0}", body.Joints[JointType.HandRight].Position.X);

                                    if (joints[JointType.HandRight].TrackingState == TrackingState.Tracked)
                                    {
                                        if (trackRightGrabAndHold(body))
                                        {
                                            gestureList.GrabAndHold.currentPoint = joints[JointType.HandRight].Position;
                                            gestureList.GrabAndHold.currentPoint.X -= (spine_shoulderVec.X + (float).25);
                                            gestureList.GrabAndHold.currentPoint.Y -= (spine_shoulderVec.Y + (float).15);
                                            sListener.onGestureRegistered(Gestures.GrabAndHold, gestureList.GrabAndHold.currentPoint);
                                            return;
                                        }

                                        if (trackRightHandSwipeLeft(body) && (gestureList.GrabAndHold.Rstatus != ListOfGestures.Status.InProgress))
                                        {
                                            readyForGesture = false;
                                            gestureList.reset();
                                            sListener.onGestureRegistered(Gestures.RightHandSwipe, gestureList.RightHandSwipe.firstPoint);
                                            return;
                                        }
                                        if (trackRightFling(body))
                                        {
                                            readyForGesture = false;
                                            gestureList.reset();
                                            sListener.onGestureRegistered(Gestures.RightFling, gestureList.RightFling.firstPoint);
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        gestureList.RightHandSwipe.reset();
                                        gestureList.RightFling.reset();
                                        //return;
                                    }
                                    if (joints[JointType.HandLeft].TrackingState == TrackingState.Tracked)
                                    {
                                        if (trackLeftGrabAndHold(body))
                                        {
                                            gestureList.GrabAndHold.currentPoint = joints[JointType.HandLeft].Position;
                                            gestureList.GrabAndHold.currentPoint.X += (spine_shoulderVec.X + (float).25);
                                            gestureList.GrabAndHold.currentPoint.Y -= (spine_shoulderVec.Y + (float).15);
                                            sListener.onGestureRegistered(Gestures.GrabAndHold, gestureList.GrabAndHold.currentPoint);
                                            return;
                                        }

                                        if (trackLeftHandSwipeRight(body) && (gestureList.GrabAndHold.Lstatus != ListOfGestures.Status.InProgress))
                                        {
                                            readyForGesture = false;
                                            gestureList.reset();
                                            sListener.onGestureRegistered(Gestures.LeftHandSwipe, gestureList.LeftHandSwipe.firstPoint);
                                            return;
                                        }
                                        if (trackLeftFling(body))
                                        {
                                            readyForGesture = false;
                                            gestureList.reset();
                                            sListener.onGestureRegistered(Gestures.LeftFling, gestureList.LeftFling.firstPoint);
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        gestureList.LeftHandSwipe.reset();
                                        gestureList.LeftFling.reset();
                                        //return;
                                    }

                                    if (trackRightPinch(body) || trackLeftPinch(body))
                                    {
                                        CameraSpacePoint trash = new CameraSpacePoint();
                                        readyForGesture = false;
                                        sListener.onGestureRegistered(Gestures.Pinch, trash);
                                    }

                                    if (changedAttention(headVec.Z - neckVec.Z, neckVec.Z - spine_shoulderVec.Z, spine_shoulderVec.Z - spine_midVec.Z, vec.W))
                                    {
                                        sListener.onAttentionChanged(currentAttention);
                                        gestureList.reset();
                                        return;
                                    }

                                    /*if (changedHandPosition(body))
                                    {
                                        sListener.onHandPositionChanged(currentHandPositions);
                                        return;
                                    }*/

                                    

                                }
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    // ignore if the frame is no longer available
                }
            }
            else
            {
                if (framesSinceGesture < 5)
                {
                    framesSinceGesture++;
                }
                else
                {
                    readyForGesture = true;
                    framesSinceGesture = 0;
                }
            }
        }
        
        private static bool trackRightHandSwipeLeft(Body body)
        {
            IReadOnlyDictionary<JointType, Joint> joints = body.Joints;
            CameraSpacePoint currentPoint = joints[JointType.HandTipRight].Position;
            IReadOnlyDictionary<JointType, JointOrientation> orientation = body.JointOrientations;
            Vector4 vec = orientation[JointType.ElbowRight].Orientation;

            int MinXDelta = 10; // required horizontal distance
            int MaxYDelta = 5; // max mount of vertical variation

            float curX = currentPoint.X * 100;
            float curY = currentPoint.Y * 100;

            gestureList.RightHandSwipe.totalFrames++;
            if (body.HandRightConfidence == TrackingConfidence.High)
            {
                gestureList.RightHandSwipe.goodFrames++;
            }

            if (gestureList.RightHandSwipe.status == ListOfGestures.Status.NotTracked)
            {
                gestureList.RightHandSwipe.firstPoint.X = curX;
                gestureList.RightHandSwipe.firstPoint.Y = curY;
                gestureList.RightHandSwipe.lastPoint.X = gestureList.RightHandSwipe.firstPoint.X;
                gestureList.RightHandSwipe.lastPoint.Y = gestureList.RightHandSwipe.firstPoint.Y;

                gestureList.RightHandSwipe.status = ListOfGestures.Status.Tracked;
                rightHandSwipeLeftWatch.Restart();

                return false;
            }

            if (rightHandSwipeLeftWatch.Elapsed.Milliseconds > 35)
            {
                gestureList.RightHandSwipe.reset();
                return false;
            }

            /*if (Math.Abs(curY - gestureList.RightHandSwipe.firstPoint.Y) > MaxYDelta)// || gestureList.RightHandSwipe.lastPoint.X - curX < 0)
            {
                gestureList.RightHandSwipe.status = ListOfGestures.Status.NotTracked;
                Console.WriteLine("Y Error");
                return false;
            }*/
            else if (joints[JointType.WristRight].Position.Z >= joints[JointType.ElbowRight].Position.Z ||
                    Math.Abs(joints[JointType.WristRight].Position.Y - joints[JointType.ElbowRight].Position.Y) > .25) // ||
                    //Math.Abs(vec.W * 10) < 2 || Math.Abs(vec.W * 10) > 5)
            {
                gestureList.RightHandSwipe.status = ListOfGestures.Status.NotTracked;
                //Console.WriteLine("Z Error");
                return false;
            }
            else if (gestureList.RightHandSwipe.firstPoint.X - curX >= MinXDelta && Math.Abs(curY - gestureList.RightHandSwipe.firstPoint.Y) <= MaxYDelta)
            {
                gestureList.RightHandSwipe.status = ListOfGestures.Status.NotTracked;
                Console.WriteLine("{0}", gestureList.RightHandSwipe.goodFrames / gestureList.RightHandSwipe.totalFrames);
                if (gestureList.RightHandSwipe.goodFrames / gestureList.RightHandSwipe.totalFrames >= 0.155)
                {
                    return true;
                }
                else
                {
                    gestureList.RightHandSwipe.reset();
                    return false;
                }
            }
            else
            {
                //Console.WriteLine("{0}", gestureList.RightHandSwipe.firstPoint.X - curX);
                gestureList.RightHandSwipe.lastPoint.X = curX;
                gestureList.RightHandSwipe.lastPoint.Y = curY;
                return false;
            }
        }

        private static bool trackLeftHandSwipeRight(Body body)
        {
            IReadOnlyDictionary<JointType, Joint> joints = body.Joints;
            CameraSpacePoint currentPoint = joints[JointType.HandTipLeft].Position;
            IReadOnlyDictionary<JointType, JointOrientation> orientation = body.JointOrientations;
            Vector4 vec = orientation[JointType.ElbowLeft].Orientation;

            int MinXDelta = 10; // required horizontal distance
            int MaxYDelta = 5; // max mount of vertical variation

            float curX = currentPoint.X * 100;
            float curY = currentPoint.Y * 100;

            gestureList.LeftHandSwipe.totalFrames++;
            if (body.HandLeftConfidence == TrackingConfidence.High)
            {
                gestureList.LeftHandSwipe.goodFrames++;
            }

            if (gestureList.LeftHandSwipe.status == ListOfGestures.Status.NotTracked)
            {
                gestureList.LeftHandSwipe.firstPoint.X = curX;
                gestureList.LeftHandSwipe.firstPoint.Y = curY;
                gestureList.LeftHandSwipe.lastPoint.X = gestureList.LeftHandSwipe.firstPoint.X;
                gestureList.LeftHandSwipe.lastPoint.Y = gestureList.LeftHandSwipe.firstPoint.Y;

                gestureList.LeftHandSwipe.status = ListOfGestures.Status.Tracked;
                leftHandSwipeRightWatch.Restart();

                return false;
            }

            if (leftHandSwipeRightWatch.Elapsed.Milliseconds > 35)
            {
                gestureList.LeftHandSwipe.reset();
                return false;
            }

            /*if (Math.Abs(curY - gestureList.LeftHandSwipe.firstPoint.Y) > MaxYDelta || curX - gestureList.LeftHandSwipe.lastPoint.X < 0)
            {
                gestureList.LeftHandSwipe.status = ListOfGestures.Status.NotTracked;
                return false;
            }*/
            else if (joints[JointType.WristLeft].Position.Z >= joints[JointType.ElbowLeft].Position.Z ||
                     Math.Abs(joints[JointType.WristLeft].Position.Y - joints[JointType.ElbowLeft].Position.Y) > .25) // ||
                    //Math.Abs(vec.W * 10) < 2 || Math.Abs(vec.W * 10) > 5)
            {
                gestureList.LeftHandSwipe.status = ListOfGestures.Status.NotTracked;
                //Console.WriteLine("Z Error");
                return false;
            }
            else if (curX - gestureList.LeftHandSwipe.firstPoint.X >= MinXDelta && Math.Abs(curY - gestureList.LeftHandSwipe.firstPoint.Y) <= MaxYDelta)
            {
                gestureList.LeftHandSwipe.status = ListOfGestures.Status.NotTracked;
                Console.WriteLine("{0}", gestureList.LeftHandSwipe.goodFrames / gestureList.LeftHandSwipe.totalFrames);
                if (gestureList.LeftHandSwipe.goodFrames / gestureList.LeftHandSwipe.totalFrames >= 0.155)
                {
                    return true;
                }
                else
                {
                    gestureList.LeftHandSwipe.reset();
                    return false;
                }
            }
            else
            {
                gestureList.LeftHandSwipe.lastPoint.X = curX;
                gestureList.LeftHandSwipe.lastPoint.Y = curY;
                return false;
            }
        }

        private static bool trackRightPinch(Body body)
        {
            if (pinchOpenRightCount < 5 && pinchClosedRightCount < 5)
            {
                if (body.HandRightState == HandState.Lasso)
                {
                    pinchOpenRightCount++;
                }
                else
                {
                    pinchOpenRightCount = 0;
                }
            }
            else
            {
                if (pinchClosedRightCount < 5)
                {
                    if (body.HandRightState == HandState.Closed)
                    {
                        pinchClosedRightCount++;
                        if (pinchClosedRightCount >= 5)
                        {
                            pinchOpenRightCount = 0;
                        }
                    }
                    else
                    {
                        pinchClosedRightCount = 0;
                    }
                }
                else
                {
                    if (pinchOpenRightCount < 5)
                    {
                        if (body.HandRightState == HandState.Lasso)
                        {
                            pinchOpenRightCount++;
                        }
                        else
                        {
                            pinchOpenRightCount = 0;
                        }
                    }
                    else
                    {
                        pinchOpenRightCount = 0;
                        pinchClosedRightCount = 0;
                        return true;
                    }
                }
            }
            return false;
        }

        private static bool trackLeftPinch(Body body)
        {
            if (pinchOpenLeftCount < 5 && pinchClosedLeftCount < 5)
            {
                if (body.HandLeftState == HandState.Lasso)
                {
                    pinchOpenLeftCount++;
                }
                else
                {
                    pinchOpenLeftCount = 0;
                }
            }
            else
            {
                if (pinchClosedLeftCount < 5)
                {
                    if (body.HandLeftState == HandState.Closed)
                    {
                        pinchClosedLeftCount++;
                        if (pinchClosedLeftCount >= 5)
                        {
                            pinchOpenLeftCount = 0;
                        }
                    }
                    else
                    {
                        pinchClosedLeftCount = 0;
                    }
                }
                else
                {
                    if (pinchOpenLeftCount < 5)
                    {
                        if (body.HandLeftState == HandState.Lasso)
                        {
                            pinchOpenLeftCount++;
                        }
                        else
                        {
                            pinchOpenLeftCount = 0;
                        }
                    }
                    else
                    {
                        pinchOpenLeftCount = 0;
                        pinchClosedLeftCount = 0;
                        return true;
                    }
                }
            }
            return false;
        }

        private static bool trackRightGrabAndHold(Body body)
        {
            if (gestureList.GrabAndHold.Rstatus == ListOfGestures.Status.NotTracked)
            {
                if (body.HandRightState == HandState.Open)
                {
                    gestureList.GrabAndHold.RopenFrameCount++;
                    return false;
                }
                else if (body.HandRightState == HandState.Closed && body.HandRightConfidence == TrackingConfidence.High && gestureList.GrabAndHold.RopenFrameCount != 0)
                {
                    gestureList.GrabAndHold.RgrabFrameCount++;
                    gestureList.GrabAndHold.Rstatus = ListOfGestures.Status.Tracked;
                    return false;
                }
                gestureList.GrabAndHold.Rreset();
                return false;
            }
            else if (gestureList.GrabAndHold.Rstatus == ListOfGestures.Status.Tracked)
            {
                if (body.HandRightState == HandState.Closed)
                {
                    if (gestureList.GrabAndHold.RgrabFrameCount++ < 8)
                    {
                        return false;
                    }
                    else
                    {
                        gestureList.GrabAndHold.Rstatus = ListOfGestures.Status.InProgress;
                        return true;
                    }
                }
                else
                {
                    gestureList.reset();
                    return false;
                }
            }
            else
            {
                if ((body.HandRightState == HandState.Open || body.HandRightState == HandState.NotTracked) && body.HandRightConfidence == TrackingConfidence.High)
                {
                    readyForGesture = false;
                    gestureList.GrabAndHold.Rreset();
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        private static bool trackLeftGrabAndHold(Body body)
        {
            if (gestureList.GrabAndHold.Lstatus == ListOfGestures.Status.NotTracked)
            {
                if (body.HandLeftState == HandState.Open)
                {
                    gestureList.GrabAndHold.LopenFrameCount++;
                    return false;
                }
                else if (body.HandLeftState == HandState.Closed && body.HandLeftConfidence == TrackingConfidence.High && gestureList.GrabAndHold.LopenFrameCount != 0)
                {
                    gestureList.GrabAndHold.LgrabFrameCount++;
                    gestureList.GrabAndHold.Lstatus = ListOfGestures.Status.Tracked;
                    return false;
                }
                gestureList.GrabAndHold.Lreset();
                return false;
            }
            else if (gestureList.GrabAndHold.Lstatus == ListOfGestures.Status.Tracked)
            {
                if (body.HandLeftState == HandState.Closed)
                {
                    if (gestureList.GrabAndHold.LgrabFrameCount++ < 8)
                    {
                        return false;
                    }
                    else
                    {
                        gestureList.GrabAndHold.Lstatus = ListOfGestures.Status.InProgress;
                        return true;
                    }
                }
                else
                {
                    gestureList.reset();
                    return false;
                }
            }
            else
            {
                if ((body.HandLeftState == HandState.Open || body.HandLeftState == HandState.NotTracked) && body.HandLeftConfidence == TrackingConfidence.High)
                {
                    readyForGesture = false;
                    gestureList.GrabAndHold.Lreset();
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        private static bool changedAttention(float data1, float data2, float data3, float neckOri)
        {
            Attention att;

            if (neckOri > 0.4)
            {
                att = Attention.LookingAway;
            }
            else if (data1 < -0.06)// && data2 < 0.005 && data3 < 0.005)
            {
                att = Attention.LookingDown;
            }
            else
            {
                att = Attention.LookingUp;
            }

            if (recentAttentionSwitch)
            {
                if (att != currentAttention)
                {
                    if (att == lastStableAttention)
                    {
                        attentionWatch.Reset();
                        recentAttentionSwitch = false;
                        currentAttention = lastStableAttention;
                    }
                    else
                    {
                        attentionWatch.Restart();
                        currentAttention = att;
                    }
                }
                else
                {
                    if (attentionWatch.Elapsed.Milliseconds >= 500 && currentAttention == Attention.LookingDown)
                    {
                        attentionWatch.Reset();
                        recentAttentionSwitch = false;
                        lastStableAttention = currentAttention;
                        return true;
                    }
                    else if (attentionWatch.Elapsed.Milliseconds >= 500 && (currentAttention == Attention.LookingUp || currentAttention == Attention.LookingAway))
                    {
                        attentionWatch.Reset();
                        recentAttentionSwitch = false;
                        lastStableAttention = currentAttention;
                        return true;
                    }
                }
            }
            else if (att != currentAttention)
            {
                attentionWatch.Start();
                currentAttention = att;
                recentAttentionSwitch = true;
            }

            return false;
        }

        private static bool trackRightFling(Body body)
        {
            IReadOnlyDictionary<JointType, Joint> joints = body.Joints;
            CameraSpacePoint currentPoint = joints[JointType.HandRight].Position;
            IReadOnlyDictionary<JointType, JointOrientation> orientation = body.JointOrientations;
            Vector4 vec = orientation[JointType.ElbowRight].Orientation;

            int MinZDelta = 5; // required horizontal distance
            int MaxXDelta = 4; // max mount of vertical variation

            float curX = currentPoint.X * 100;
            float curZ = currentPoint.Z * 100;

            gestureList.RightFling.totalFrames++;
            if (body.HandRightConfidence == TrackingConfidence.High)
            {
                gestureList.RightFling.goodFrames++;
            }

            if (gestureList.RightFling.status == ListOfGestures.Status.NotTracked && Math.Abs(curX - (body.Joints[JointType.SpineShoulder].Position.X * 100)) < 10)
            {
                gestureList.RightFling.firstPoint.X = curX;
                gestureList.RightFling.firstPoint.Z = curZ;
                gestureList.RightFling.lastPoint.X = gestureList.RightFling.firstPoint.X;
                gestureList.RightFling.lastPoint.Z = gestureList.RightFling.firstPoint.Z;

                gestureList.RightFling.status = ListOfGestures.Status.Tracked;
                rightFlingWatch.Restart();

                return false;
            }

            else if (rightFlingWatch.Elapsed.Milliseconds > 35)
            {
                gestureList.RightFling.reset();
                return false;
            }

            else if (Math.Abs(curX - gestureList.RightFling.firstPoint.X) > MaxXDelta || gestureList.RightFling.lastPoint.Z - curZ < 0)
            {
                gestureList.RightFling.reset();
                return false;
            }
            /*else if (joints[JointType.WristRight].Position.Z >= joints[JointType.ElbowRight].Position.Z ||
                    Math.Abs(joints[JointType.WristRight].Position.Y - joints[JointType.ElbowRight].Position.Y) > .13) // ||
            //Math.Abs(vec.W * 10) < 2 || Math.Abs(vec.W * 10) > 5)
            {
                gestureList.Fling.status = ListOfGestures.Status.NotTracked;
                //Console.WriteLine("Z Error");
                return false;
            }*/
            else if (gestureList.RightFling.firstPoint.Z - curZ >= MinZDelta)
            {
                gestureList.RightFling.status = ListOfGestures.Status.NotTracked;
                Console.WriteLine("{0}", (float)(gestureList.RightFling.goodFrames / gestureList.RightFling.totalFrames));
                if (gestureList.RightFling.goodFrames / gestureList.RightFling.totalFrames >= 0.2)
                {
                    return true;
                }
                else
                {
                    gestureList.RightFling.reset();
                    return false;
                }
            }
            else
            {
                gestureList.RightFling.lastPoint.X = curX;
                gestureList.RightFling.lastPoint.Z = curZ;
                return false;
            }
        }

        private static bool trackLeftFling(Body body)
        {
            IReadOnlyDictionary<JointType, Joint> joints = body.Joints;
            CameraSpacePoint currentPoint = joints[JointType.HandTipLeft].Position;
            IReadOnlyDictionary<JointType, JointOrientation> orientation = body.JointOrientations;
            Vector4 vec = orientation[JointType.ElbowLeft].Orientation;

            int MinZDelta = 5; // required horizontal distance
            int MaxXDelta = 4; // max mount of vertical variation

            float curX = currentPoint.X * 100;
            float curZ = currentPoint.Z * 100;

            gestureList.LeftFling.totalFrames++;
            if (body.HandLeftConfidence == TrackingConfidence.High)
            {
                gestureList.LeftFling.goodFrames++;
            }
            //Console.WriteLine("{0}", Math.Abs(curX - (body.Joints[JointType.SpineShoulder].Position.X * 100)));
            if (gestureList.LeftFling.status == ListOfGestures.Status.NotTracked && Math.Abs(curX - (body.Joints[JointType.SpineShoulder].Position.X * 100)) < 10)
            {
                gestureList.LeftFling.firstPoint.X = curX;
                gestureList.LeftFling.firstPoint.Z = curZ;
                gestureList.LeftFling.lastPoint.X = gestureList.LeftFling.firstPoint.X;
                gestureList.LeftFling.lastPoint.Z = gestureList.LeftFling.firstPoint.Z;

                gestureList.LeftFling.status = ListOfGestures.Status.Tracked;
                leftFlingWatch.Restart();

                return false;
            }

            if (leftFlingWatch.Elapsed.Milliseconds > 35)
            {
                gestureList.LeftFling.reset();
                return false;
            }

            else if (Math.Abs(curX - gestureList.LeftFling.firstPoint.X) > MaxXDelta || gestureList.LeftFling.lastPoint.Z - curZ < 0)
            {
                gestureList.LeftFling.reset();
                return false;
            }
            /*else if (joints[JointType.WristRight].Position.Z >= joints[JointType.ElbowRight].Position.Z ||
                    Math.Abs(joints[JointType.WristRight].Position.Y - joints[JointType.ElbowRight].Position.Y) > .13) // ||
            //Math.Abs(vec.W * 10) < 2 || Math.Abs(vec.W * 10) > 5)
            {
                gestureList.LeftFling.status = ListOfGestures.Status.NotTracked;
                //Console.WriteLine("Z Error");
                return false;
            }*/
            else if (gestureList.LeftFling.firstPoint.Z - curZ >= MinZDelta)
            {
                gestureList.LeftFling.status = ListOfGestures.Status.NotTracked;
                Console.WriteLine("{0}", (float)(gestureList.LeftFling.goodFrames / gestureList.LeftFling.totalFrames));
                if (gestureList.LeftFling.goodFrames / gestureList.LeftFling.totalFrames >= 0.2)
                {
                    return true;
                }
                else
                {
                    gestureList.LeftFling.reset();
                    return false;
                }
            }
            else
            {
                gestureList.LeftFling.lastPoint.X = curX;
                gestureList.LeftFling.lastPoint.Z = curZ;
                return false;
            }
        }

        private static bool changedHandPosition(Body body)
        {
            IReadOnlyDictionary<JointType, Joint> joints = body.Joints;
            CameraSpacePoint rightHand = joints[JointType.HandRight].Position;
            CameraSpacePoint leftHand = joints[JointType.HandLeft].Position;

            Hands thisHand;
            bool rightStatus = false;
            bool leftStatus = false;

            if (Math.Abs(rightHand.X*100) <= tableLength/2 && rightHand.Z*100 >= tableDistance && rightHand.Z*100 <= tableDistance+tableDepth)
            {
                rightStatus = true;
            }
            if (Math.Abs(leftHand.X*100) <= tableLength/2 && leftHand.Z*100 >= tableDistance && leftHand.Z*100 <= tableDistance+tableDepth)
            {
                leftStatus = true;
            }

            if (rightStatus && leftStatus)
            {
                thisHand = Hands.BothOverTable;
            }
            else if (rightStatus)
            {
                thisHand = Hands.RightOverTable;
            }
            else if (leftStatus)
            {
                thisHand = Hands.LeftOverTable;
            }
            else
            {
                thisHand = Hands.NotOverTable;
            }

            if (recentHandSwitch)
            {
                if (thisHand != currentHandPositions)
                {
                    if (thisHand == lastStableHandPosition)
                    {
                        handWatch.Reset();
                        recentHandSwitch = false;
                        currentHandPositions = lastStableHandPosition;
                    }
                    else
                    {
                        handWatch.Restart();
                        currentHandPositions = thisHand;
                    }
                }
                else
                {
                    if (handWatch.Elapsed.Milliseconds >= 400 && currentHandPositions != Hands.NotOverTable)
                    {
                        handWatch.Reset();
                        recentHandSwitch = false;
                        lastStableHandPosition = currentHandPositions;
                        return true;
                    }
                    else if (handWatch.Elapsed.Seconds >= 1 && handWatch.Elapsed.Milliseconds >= 250 && currentHandPositions == Hands.NotOverTable)
                    {
                        handWatch.Reset();
                        recentHandSwitch = false;
                        lastStableHandPosition = currentHandPositions;
                        return true;
                    }
                }
            }
            else if (thisHand != currentHandPositions)
            {
                handWatch.Restart();
                recentHandSwitch = true;
                currentHandPositions = thisHand;
            }
            
            return false;
        }
    }
}