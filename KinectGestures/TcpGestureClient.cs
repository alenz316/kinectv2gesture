﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Kinect;


using System.IO;
using System.Net;
using System.Net.Sockets;

namespace KinectGestures
{
    class TcpGuestureClient
    {

        static void Main(string[] args)
        {

            try
            {
                TcpClient tcpclnt = new TcpClient();
                Console.WriteLine("Connecting.....");

                tcpclnt.Connect("127.0.0.1", 14031);
                // use the ipaddress as in the server program

                Console.WriteLine("Connected");

                Stream s = tcpclnt.GetStream();

                ASCIIEncoding asen = new ASCIIEncoding();
                byte[] ba = asen.GetBytes("g_h\n");
                Console.WriteLine("Transmitting.....");

                s.Write(ba, 0, ba.Length);

            
                /// <summary>
                /// Instance of the gesture library
                /// </summary>
                GestureLibrary.setListener(new TcpGestureBroadcaster(s));

                if (GestureLibrary.start())
                {
                    // set the status text
                    Console.WriteLine("\nConnected Sensor\n");
                }
                else
                {
                    // on failure, set the status text
                    Console.WriteLine("\nDisconnected Sensor\n");
                }

                //Console.WriteLine("\nGetting Events\n");
                GestureLibrary.getEvents();
                
                //gestureLib.close();

                //tcpclnt.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error..... " + e.StackTrace);
            }
        }
    }

    class TcpGestureBroadcaster : EventListener
    {
        private Stream mStream;
        private ASCIIEncoding mEncoder;
       public  TcpGestureBroadcaster(Stream s)
        {
            mStream = s;
            mEncoder = new ASCIIEncoding();
        }

       private void transmit(String s)
       {
           byte[] ba = mEncoder.GetBytes("g_"+s+"\n");

           mStream.Write(ba, 0, ba.Length);
       }

        void EventListener.onGestureRegistered(Gestures g, CameraSpacePoint c)
        {
            if (g == Gestures.GrabAndHold)
            {
                //Console.Clear();
                //Console.WriteLine("\n{0}\n", g);
                Console.WriteLine("\nX: {0}, \tY: {1}, \tZ: {2}", c.X, c.Y, c.Z);

                transmit(g.ToString() + ";" + c.X + ";" + c.Y);
            }
            else
            {
                Console.WriteLine("\n{0}\n", g);

                transmit(g.ToString());
            }
            
            
        }
        void EventListener.onPositionChanged(Positions p)
        {

        }
        void EventListener.onAttentionChanged(Attention a)
        {
            Console.WriteLine("\n{0}\n", a);
            transmit(a.ToString());
        }

        void EventListener.onHandPositionChanged(Hands h)
        {
            //Console.WriteLine("{0}", h);
            //overTable = h;
        }
    }
}
