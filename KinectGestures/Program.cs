﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Kinect;

namespace KinectGestures
{
    class Program
    {
        public static Hands overTable;
        public static Attention attention;
        public static Positions position;

        class MyListener : EventListener
        {
            void EventListener.onGestureRegistered(Gestures g, CameraSpacePoint c)
            {
                //Console.Clear();
                Console.WriteLine("{0}", g);
                //Console.WriteLine("X: {0}, \tY: {1}, \tZ: {2}", c.X, c.Y, c.Z);
                
            }
            void EventListener.onPositionChanged(Positions p)
            {
                position = p;
            }
            void EventListener.onAttentionChanged(Attention a)
            {
                Console.WriteLine("{0}", a);
                attention = a;
            }
            void EventListener.onHandPositionChanged(Hands h)
            {
                Console.WriteLine("{0}", h);
                overTable = h;
            }
        }
        static void OldMain(string[] args)
        {
            /// <summary>
            /// Instance of the gesture library
            /// </summary>
            GestureLibrary.setListener(new MyListener());

            if (GestureLibrary.start())
            {
                // set the status text
                Console.WriteLine("\nConnected Sensor\n");
            }
            else
            {
                // on failure, set the status text
                Console.WriteLine("\nDisconnected Sensor\n");
            }

            //Console.WriteLine("\nGetting Events\n");
            GestureLibrary.getEvents();

            //gestureLib.close();
        }
    }

    
}
